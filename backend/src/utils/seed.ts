import axios from 'axios'
import { getManager } from "typeorm";
import Character from '../entities/character'
import Book from '../entities/book'
import PageRank from '../entities/pagerank'
import Title from '../entities/title'

export const seed = async () => {
    try {
        const response = await axios.get('https://api.got.show/api/book/characters');
        const charRepositorio = getManager().getRepository(Character);
        const bookRepositorio = getManager().getRepository(Book);
        let flagError = true;
        for (const element of response.data) {
            let newP: Character = new Character();
            let newT: Title[] = [];
            let newB: Book[] = [];
            let newPR: PageRank = new PageRank();
            newP.id = element.id;
            newP.gender = element.gender;
            newP.house = element.house;
            newP.image = element.image;
            newP.name = element.name;
            newP.slug = element.slug;
            newPR.rank = (element.pagerank) ? element.pagerank.rank : null;
            newPR.title = (element.pagerank) ? element.pagerank.title : null;
            for (const iterator of element.books) {
                    if (flagError && newP.id === "5cc08e61888dfb00103cd685" && iterator === "Fire & Blood") {
                        console.log("Este personaje tiene libros duplicados")
                        /* 
                            Esto es debido a que este personaje tiene libros duplicados en la api y rompe el esquema
                            autor ->(1,n)->Libros Primary key(autorId,libroIs)
                            books": [
                                "The World of Ice & Fire",
                                "Fire & Blood",
                                "The Rogue Prince",
                                "The Princess and the Queen",
                                "Fire & Blood"
                            ],
                            "id": "5cc08e61888dfb00103cd685",
                        */
                        flagError = false
                    } else {

                        let aux: Book;
                        let flag = false;
                        try {
                            aux = await bookRepositorio.findOneOrFail({ where: { name: iterator } })
                            flag = true;
                        } catch (error) {
                            flag = false;
                        }
                        if (!flag && iterator!="") {
                            aux = new Book();
                            aux.name = iterator;
                            try {

                                aux = await bookRepositorio.save(aux)
                            } catch (error) {
                                console.log(error)
                            }
                            newB.push(aux)
                        }
                    }
            }
            newP.pagerank = newPR;
            newP.books = newB;
            for (const iterator of element.titles) {
                let aux: Title = new Title();
                aux.name = iterator;
                newT.push(aux)
            }
            newP.title = newT;
            try {
                await charRepositorio.save(newP)
            } catch (error) {
                console.log(error)
            }
        };
    } catch (exception) {
        console.log(exception)
        console.log('Error')
    }
}