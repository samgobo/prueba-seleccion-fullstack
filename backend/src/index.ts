import "reflect-metadata";
import express from 'express';
import { createConnection } from "typeorm";
import { itemsRouter } from "./routes/characters";
require('dotenv').config({ path: 'variables.env' });
import {seed} from './utils/seed'
async function main() {
  let connection;
  try {
    connection = await createConnection({
      type: "sqlite",
      database: "./db.sqlite3",
      entities: ["src/entities/**/*.ts"],
      synchronize:true
    });
    await seed();
    console.log('Seed Saved')
    const app = express();
    app.use((req, res, next) => {
      res.header('Access-Control-Allow-Origin', '*');
      next();
    });
    app.use(express.json())
    app.use('/characters', itemsRouter)
    app.listen(process.env.PORT || 3005);
    console.log(`Serving http://localhost:${process.env.PORT} for ${process.env.NODE_ENV}.`);
  } catch (error) {
    console.log(error)
  }
}

main()