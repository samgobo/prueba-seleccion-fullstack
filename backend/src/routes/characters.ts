import express from 'express';
import { getCharacter, getCharacters } from '../controllers/archivosController';
export const itemsRouter = express.Router();


itemsRouter.get('/:id',
    getCharacter
)
itemsRouter.get('/',
    getCharacters
)
