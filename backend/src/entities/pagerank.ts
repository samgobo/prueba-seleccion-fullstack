import { Entity, PrimaryGeneratedColumn, Column, OneToOne } from "typeorm";
import Character from './character'

@Entity()
export default class PageRank {

    @PrimaryGeneratedColumn()
    id: string;

    @Column({name:'title', type: "varchar", nullable: true })
    title?: string;

    @Column({name:'rank', type: "varchar", nullable: true })
    rank?: string;
    
    @OneToOne(() => Character, pj => pj.pagerank)
    character: Character[];
}