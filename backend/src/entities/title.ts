import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from "typeorm";
import Character from './character'

@Entity()
export default class Title {

    @PrimaryGeneratedColumn()
    id: string;

    @Column()
    name: string;
    
    @ManyToOne(() => Character, cr => cr.title)
    character: Character;
}