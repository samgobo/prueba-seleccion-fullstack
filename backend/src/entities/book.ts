import { Entity, PrimaryGeneratedColumn, Column, ManyToMany } from "typeorm";
import Character from './character'

@Entity()
export default class Book {

    @PrimaryGeneratedColumn()
    id: string;

    @Column()
    name: string;
    
    @ManyToMany(() => Character, cr => cr.books)
    character: Character[];
}