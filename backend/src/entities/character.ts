import { Entity, PrimaryColumn, Column, OneToMany, OneToOne, JoinColumn,ManyToMany, JoinTable } from "typeorm";
import Book from "./book";
import PageRank from './pagerank'
import Title from './title'

@Entity()
export default class Character {

    @PrimaryColumn()
    id: string;

    @Column()
    name: string;

    @Column()
    slug: string;

    @Column({name:'gender', type: "varchar", nullable: true })
    gender?: string;

    @Column({name:'image', type: "varchar", nullable: true })
    image?: string;

    @Column({name:'house', type: "varchar", nullable: true })
    house?: string;

    @OneToOne(type => PageRank, pr => pr.character,{
        cascade: true,
        eager: true, nullable:true
    })
    @JoinColumn()
    pagerank?: PageRank;

    @OneToMany(() => Title, tt => tt.character, {
        cascade: true, eager: true, nullable:true
    })
    title?: Title[];

    @ManyToMany(() => Book, bk => bk.character, {
        eager: true, nullable:true
    })
    @JoinTable()
    books?: Book[];
}
