import { Request, Response } from "express";
import { getManager } from "typeorm";
import Character from '../entities/character'

export const getCharacter = async (req: Request, res: Response) => {

    try {
        const id = req.params.id;
        const charRepositorio = getManager().getRepository(Character);
        const char = await charRepositorio.findOneOrFail(id);
        return res.status(200).send(char)
    } catch (error) {
        return res.status(400).json({
            estatus: false,
            message: 'Error inesperado'
        })
    }
}
export const getCharacters = async (req: Request, res: Response) => {
    try {
        const skip = (req.query.skip) ? parseInt(req.query.skip.toString()) : 0;
        const name = req.query.name || "";
        const house = req.query.house || "";
        const charRepositorio = getManager().getRepository(Character);
        const [values, count] = await charRepositorio.createQueryBuilder("char")
            .leftJoinAndSelect("char.books", "bks")
            .leftJoinAndSelect("char.title", "tts")
            .leftJoinAndSelect("char.pagerank", "prk")
            .take(10)
            .skip(skip)
            .where(`char.name LIKE '${name}%' AND char.house LIKE '${house}%'`, { name, house })
            .getManyAndCount();
        return res.status(200).send({ values, count })
    } catch (error) {
        console.log(error)
        return  res.status(400).json({
            estatus: false,
            message: 'Error inesperado'
        })
    }

}