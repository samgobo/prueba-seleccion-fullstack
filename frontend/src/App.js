import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";
import List from './components/List'
import Details from './components/Details'
import Layout from './components/Layout'

function App() {
  return (
    <Router>
      <Layout>
        <Switch>
          <Route exact path="/details/:id">
            <Details />
          </Route>
          <Route exact path="/list">
            <List />
          </Route>
          <Route path="/">
            <Redirect to="/list" />
          </Route>
        </Switch>
      </Layout>
    </Router>
  );
}

export default App;
