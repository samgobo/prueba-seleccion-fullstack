import React, { useState } from 'react';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    root: {
        marginTop:'2rem'
    }
  }));

const Layout = (props) => {
    const classes = useState();
    return (
        <Grid
            container
            direction="row"
            justify="center"
            alignItems="center"
            classes={{
                root:classes.root
            }}
        >
        {props.children}
        </Grid>
    );
}

export default Layout;