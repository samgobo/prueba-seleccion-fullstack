import React from 'react';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import Alert from '@material-ui/lab/Alert';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        marginTop:'2rem'
    },
    paper: {
        height: 140,
        width: 100,
    },
    control: {
        padding: theme.spacing(2),
    },
}));

const Error = () => {
    const classes = useStyles();
    return (
        <Grid container className={classes.root} spacing={2} direction="row"
            justify="center"
            alignItems="center">
            <Grid item xs={10}>
                <Alert severity="error">Error inesperado!</Alert>
            </Grid>
        </Grid>
    );
}

export default Error;