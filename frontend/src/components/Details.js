import React, { useEffect, useReducer } from 'react';
import { useParams } from "react-router-dom";
import Progress from './Progress'
import Grid from '@material-ui/core/Grid';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import { styled } from '@material-ui/core/styles';
import { Link } from 'react-router-dom'
import Chip from '@material-ui/core/Chip';
import Error from './Error';
const useStyles = makeStyles((theme) => ({
    imageC: {
        maxHeight: '250px'
    }
}));
export const MuiLink = styled(Link)({
    textDecoration: 'none',
    color: 'inherit',
    display: 'inherit',
    marginTop: '2rem',
    marginLeft: '1rem'
});


function reducer(state, action) {
    switch (action.type) {
        case 'addData':
            return { ...state, loading: false, error: false, data: action.data };
        case 'addError':
            return { ...state, error: true, loading: false };
        default:
            return state
    }
}
const initialState = {
    loading: true,
    error: false,
    data: {}
};
const Details = () => {
    const classes = useStyles();
    let { id } = useParams();
    const [state, dispatch] = useReducer(reducer, initialState);
    useEffect(() => {
        fetch(
            `http://localhost:4002/characters/${id}`,
            {
                method: 'GET',
                mode: 'cors'
            }
        )
            .then(response => {
                if (!response.ok) {
                    dispatch({ type: 'addError' })
                    throw new Error("Not 2xx response")
                }
                return response.json()
            }
            )
            .then(json => {
                dispatch({ type: 'addData', data: json })
            })
            .catch(error => console.log(error));
    }, [])
    if (state.loading) return <Progress />
    if (state.error) return <Error />
    const nr = 'No Registrado'
    return (
        <Grid container>
            <Grid>
                <MuiLink to={`/list`}><ArrowBackIcon /></MuiLink>
            </Grid>
            <Grid item xs={12}>
                <Typography variant="h3" gutterBottom align='center'>
                    Detaller
            </Typography>
            </Grid>
            <TableContainer >
                <Table aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            <TableCell align="left">Campo</TableCell>
                            <TableCell align="right">Valor</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        <TableRow>
                            <TableCell align="left">Nombre</TableCell>
                            <TableCell align="right">{state.data.name}</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell align="left">Genero</TableCell>
                            <TableCell align="right">{(state.data.gender) ? state.data.gender : nr}</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell align="left">Slug</TableCell>
                            <TableCell align="right">{(state.data.slug) ? state.data.slug : nr}</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell align="left">Casa</TableCell>
                            <TableCell align="right">{(state.data.house) ? state.data.house : nr}</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell align="left">Rank</TableCell>
                            <TableCell align="right">{(state.data.pagerank) ? state.data.pagerank.rank : nr}</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell align="left">Titulos</TableCell>
                            <TableCell align="right">{(state.data.title) ? state.data.title.map(element =>
                                <Chip label={element.name} key={element.id} />
                            ) : nr}</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell align="left">Libros</TableCell>
                            <TableCell align="right">{(state.data.books) ? state.data.books.map(element =>
                                <Chip label={element.name} key={element.id} />
                            ) : nr}</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell align="left">Imagen</TableCell>
                            <TableCell align="right">
                                <div className={classes.imageC}>
                                    {(state.data.image) ? <img src={state.data.image} className={classes.imageC} alt="Image" /> : nr}
                                </div>
                            </TableCell>
                        </TableRow>
                    </TableBody>
                </Table>
            </TableContainer>
        </Grid>
    );
}

export default Details;