import React, { useEffect, useReducer } from 'react';
import Progress from './Progress'
import Error from './Error'
import Grid from '@material-ui/core/Grid';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import { styled } from '@material-ui/core/styles';
import { Link } from 'react-router-dom'
import VisibilityIcon from '@material-ui/icons/Visibility';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import TextField from '@material-ui/core/TextField';
const initialState = {
    loading: true,
    skip: 0,
    name: "",
    house: "",
    count: 0,
    error: false,
    data: []
};
const useStyles = makeStyles((theme) => ({
    rootFilter: {
        marginTop: '2rem',
        marginBottom: '2rem',
    },
}));
function reducer(state, action) {
    switch (action.type) {
        case 'addData':
            return { ...state, loading: false, error: false, data: action.data, count: action.count };
        case 'addError':
            return { ...state,loading: false, error: true };
        case 'addParameter': {
            let toAdd = {}
            const name = action.name;
            toAdd = { [name]: action.value }
            return { ...state, ...toAdd, skip: 0 };
        }
        case 'addSkip': return { ...state, skip: state.skip + 1 };
        case 'subtSkip': return { ...state, skip: state.skip - 1 };
        default:
            return state
    }
}

const StyledTableCell = withStyles((theme) => ({
    head: {
        backgroundColor: theme.palette.common.black,
        color: theme.palette.common.white,
    },
    body: {
        fontSize: 14,
    },
}))(TableCell);
export const MuiLink = styled(Link)({
    textDecoration: 'none',
    color: 'inherit',
    display: 'inherit',
});
const List = () => {
    const classes = useStyles();
    const [state, dispatch] = useReducer(reducer, initialState);
    const handleChangeValue = event => {
        const { name, value } = event.target;
        dispatch({ type: 'addParameter', name, value })
    };
    let totalPag = state.count / 10;
    if (totalPag <= 1) {
        totalPag = 1;
    } else {
        totalPag = parseInt(totalPag)
        let auxT = state.count % 10;
        if (auxT !== 0) {
            totalPag = totalPag + 1;
        }
    }
    let skip2 = state.skip + 1;
    useEffect(() => {
        fetch(
            `http://localhost:4002/characters?name=${state.name}&house=${state.house}&skip=${state.skip * 10}`,
            {
                method: 'GET',
                mode: 'cors'
            }
        )
            .then(response => {
                if (!response.ok) {
                    dispatch({type:'addError'})
                    throw new Error("Not 2xx response")
                }
                return response.json()
            })
            .then(json => {
                dispatch({ type: 'addData', data: json.values, count: json.count })
            })
            .catch(error => {
                console.log(error)
            });
    }, [state.skip, state.name, state.house])
    if (state.loading) return <Progress />
    if (state.error) return <Error />
    return (

        <Grid
            container
            direction="row"
            justify="center"
            alignItems="center"
        >
            <Grid item xs={12}>
                <Typography variant="h3" gutterBottom align='center'>
                    Lista de Personajes
            </Typography>
            </Grid>
            <Grid item xs={12} classes={{ root: classes.rootFilter }}>
                <Grid container alignItems='center' justify='center' spacing={2}>
                    <Grid item xs={4} >
                        <TextField label="Nombre" name='name' defaultValue="" fullWidth onChange={handleChangeValue} />
                    </Grid>
                    <Grid item xs={4}>
                        <TextField label="Casa" name='house' defaultValue="" fullWidth onChange={handleChangeValue} />
                    </Grid>
                </Grid>
            </Grid>
            <Grid item xs={12}>
                <TableContainer>
                    <Table size="small" aria-label="a dense table">
                        <TableHead>
                            <TableRow>
                                <StyledTableCell>Nombre</StyledTableCell>
                                <StyledTableCell align="right">Genero</StyledTableCell>
                                <StyledTableCell align="right">Casa</StyledTableCell>
                                <StyledTableCell align="left">Ver</StyledTableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {state.data.map((row) => (
                                <TableRow key={row.id}>
                                    <StyledTableCell component="th" scope="row">
                                        {row.name}
                                    </StyledTableCell>
                                    <StyledTableCell align="right">{row.gender}</StyledTableCell>
                                    <StyledTableCell align="right">{row.house}</StyledTableCell>
                                    <StyledTableCell align="right"><MuiLink to={`details/${row.id}`}><VisibilityIcon /></MuiLink></StyledTableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
                <Grid item xs={12}>
                    <Grid container alignItems="center" justify="center">
                        {
                            (state.skip < 1) ? '' :
                                <IconButton
                                    onClick={(e) => { dispatch({ type: "subtSkip" }) }}

                                >
                                    <ChevronLeftIcon />
                                </IconButton>
                        }
                        {skip2} de {totalPag}
                        {(skip2 >= totalPag) ? '' :
                            < IconButton
                                onClick={(e) => { dispatch({ type: "addSkip" }) }}
                            >
                                <ChevronRightIcon />
                            </IconButton>
                        }
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    );
}

export default List;