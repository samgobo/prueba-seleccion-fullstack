import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        marginTop:'2rem'
    },
    paper: {
        height: 140,
        width: 100,
    },
    control: {
        padding: theme.spacing(2),
    },
}));

const Progress = () => {
    const classes = useStyles();
    return (
        <Grid container className={classes.root} spacing={2} direction="row"
            justify="center"
            alignItems="center">
            <CircularProgress />
        </Grid>
    );
}

export default Progress;